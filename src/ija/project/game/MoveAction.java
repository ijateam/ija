/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.project.game;

import java.io.Serializable;

/**
 *
 * @author zemek
 */
public class MoveAction implements Serializable{
    private final int row;
    private final int col;

    /**
     * Vytvori akci posunu hrace pro ulozeni
     * @param row radek
     * @param col sloupec
     */
    public MoveAction(int row, int col) {
        this.row = row;
        this.col = col;
    }

    /**
     * Vrati radek
     * @return radek
     */
    public int getRow() {
        return row;
    }

    /**
     * Vrati sloupec
     * @return sloupec
     */
    public int getCol() {
        return col;
    }
    
    
}
