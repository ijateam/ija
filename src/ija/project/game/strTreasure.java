/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.project.game;

import ija.project.treasure.TreasureCard;
import java.io.Serializable;

/**
 *
 * @author zemek
 */
public class strTreasure implements Serializable{

    private final int row;
    private final int col;
    private TreasureCard card;

    /**
     * Vytvori poklad pro ulozeni
     * @param card karta s pokladem
     * @param row radek
     * @param col sloupec
     */
    public strTreasure(TreasureCard card, int row, int col) {
        this.row = row;
        this.col = col;
        this.card = card;
    }

    /**
     * Vrati radek
     * @return radek
     */
    public int getRow() {
        return row;
    }

    /**
     * Vrati sloupec
     * @return sloupec
     */
    public int getCol() {
        return col;
    }
    
    /** 
     * Vrati kartu s pokladem
     * @return karta s pokladem
     */
    public TreasureCard getCard() {
        return card;
    }

  
    
}
