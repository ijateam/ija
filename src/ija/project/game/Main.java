/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.project.game;

import ija.project.board.MazeBoard;
import javax.swing.SwingUtilities;

/**
 *
 * @author Martin Zemek, xzemek04
 */
public class Main {

    public static void main(String[] args) {
        int n = 7;
        MazeBoard mb;
        mb = MazeBoard.createMazeBoard(n);

        Gui g = new Gui();
        SwingUtilities.invokeLater(g);

    }

}
