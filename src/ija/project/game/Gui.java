/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.project.game;

import java.awt.event.*;
import java.awt.Dimension;
import javax.swing.*;

import ija.project.board.*;
import ija.project.treasure.*;

/**
 *
 * @author Martin Zemek, xzemek04
 */
public class Gui extends javax.swing.JFrame implements Runnable {

    private int size;
    private int noP;
    private int noT;
    private MazeBoard mb;
    private ImageIcon[] images;
    private ImageIcon[] pImages;
    private ImageIcon[] tImages;
    private int actualP;
    private int actualM;
    
    private int imageSize = 64;

    /**
     * Vytvori Gui
     */
    public Gui() {
        setImages();
        initComponents();
        this.size = 0;
        this.noP = 0;
        this.noT = 0;
        this.actualP = 0;
        this.actualM = 0;
    }
    
    /**
     * Nastavi cesty k obrazkum
     */
    private void setImages() {
        this.images = new ImageIcon[10];
        
        images[0] = new ImageIcon("../lib/paths/I1.png");
        images[1] = new ImageIcon("../lib/paths/I2.png");
        images[2] = new ImageIcon("../lib/paths/L1.png");
        images[3] = new ImageIcon("../lib/paths/L2.png");
        images[4] = new ImageIcon("../lib/paths/L3.png");
        images[5] = new ImageIcon("../lib/paths/L4.png");
        images[6] = new ImageIcon("../lib/paths/T1.png");
        images[7] = new ImageIcon("../lib/paths/T2.png");
        images[8] = new ImageIcon("../lib/paths/T3.png");
        images[9] = new ImageIcon("../lib/paths/T4.png");

        this.pImages = new ImageIcon[4];
        pImages[0] = new ImageIcon("../lib/P1.png");
        pImages[1] = new ImageIcon("../lib/P2.png");
        pImages[2] = new ImageIcon("../lib/P3.png");
        pImages[3] = new ImageIcon("../lib/P4.png");

        this.tImages = new ImageIcon[24];
        tImages[0] = new ImageIcon("../lib/treasures/ant1.png");
        tImages[1] = new ImageIcon("../lib/treasures/bear13.png");
        tImages[2] = new ImageIcon("../lib/treasures/bird58.png");
        tImages[3] = new ImageIcon("../lib/treasures/butterfly89.png");
        tImages[4] = new ImageIcon("../lib/treasures/camel2.png");
        tImages[5] = new ImageIcon("../lib/treasures/crocodile1.png");
        tImages[6] = new ImageIcon("../lib/treasures/deer4.png");
        tImages[7] = new ImageIcon("../lib/treasures/domestic.png");
        tImages[8] = new ImageIcon("../lib/treasures/elephant6.png");
        tImages[9] = new ImageIcon("../lib/treasures/flamingo3.png");
        tImages[10] = new ImageIcon("../lib/treasures/gecko2.png");
        tImages[11] = new ImageIcon("../lib/treasures/gorgosaurus.png");
        tImages[12] = new ImageIcon("../lib/treasures/hawk.png");
        tImages[13] = new ImageIcon("../lib/treasures/kangaroo.png");
        tImages[14] = new ImageIcon("../lib/treasures/monkey3.png");
        tImages[15] = new ImageIcon("../lib/treasures/owl13.png");
        tImages[16] = new ImageIcon("../lib/treasures/parrot.png");
        tImages[17] = new ImageIcon("../lib/treasures/rabbit5.png");
        tImages[18] = new ImageIcon("../lib/treasures/scorpion5.png");
        tImages[19] = new ImageIcon("../lib/treasures/seahorse1.png");
        tImages[20] = new ImageIcon("../lib/treasures/snake3.png");
        tImages[21] = new ImageIcon("../lib/treasures/swift.png");
        tImages[22] = new ImageIcon("../lib/treasures/turtle1.png");
        tImages[23] = new ImageIcon("../lib/treasures/wild4.png");
        
    }
    
    /**
     * Vykreslovaci krok
     * Prekresluje herni pole, zvetsuje zmensuje velikost pole dle potreby
     * Umistuje herni karty, poklady a hrace
     * Aktualizuje informace o aktualni hre
     */
    private void step() {
        jPanel1.removeAll();
        jPanel1.revalidate();
        jPanel1.repaint();

        freeCard.removeAll();
        freeCard.revalidate();
        freeCard.repaint();

        jPanel1.setLayout(new java.awt.GridLayout(size, size));
        jPanel1.setPreferredSize(new Dimension(size * imageSize + (size - 1), size * imageSize + (size - 1)));

        int pom = this.actualP + 1;
        playerOnMove2.setText("Hráč " + pom);
        switch (this.actualM) {
            case 0:
                awaitedMove2.setText("Vložení volného kamene");
                break;
            case 1:
                awaitedMove2.setText("Pohyb hracího kamene");
                break;
        }

        JLabel f;
        String type;
        int tmpDir;
        TreasureCard treasure;
        JLayeredPane layeredPane;

        // volna karta
        type = this.mb.getFreeCard().getType();
        tmpDir = this.mb.getFreeCard().getDir();
        treasure = this.mb.getFreeCard().getTreasure();

        layeredPane = new JLayeredPane();
        layeredPane.setName("freeL");

        switch (type) {
            case "I":
                tmpDir = 0 + tmpDir;
                f = new JLabel(images[tmpDir]);
                break;
            case "L":
                tmpDir = 2 + tmpDir;
                f = new JLabel(images[tmpDir]);
                break;
            case "T":
                tmpDir = 6 + tmpDir;
                f = new JLabel(images[tmpDir]);
                break;
            default:
                f = new JLabel(images[0]);
                break;
        }

        if (treasure != null) {
            JLabel t = new JLabel(tImages[treasure.getTCcode()]);
            t.setBounds(8, 8, imageSize-16, imageSize-16);
            t.setVisible(true);
            layeredPane.add(t, 2);
        }

        f.setName("free");
        f.setBounds(0, 0, imageSize, imageSize);
        f.setVisible(true);

        layeredPane.add(f, 1);
        layeredPane.setVisible(true);

        freeCard.add(layeredPane);
        freeCard.revalidate();
        freeCard.repaint();

        // hraci pole
        JLabel c;

        for (int rowIndex = 0; rowIndex < size; rowIndex++) {
            for (int colIndex = 0; colIndex < size; colIndex++) {
                type = this.mb.get(rowIndex + 1, colIndex + 1).getCard().getType();
                tmpDir = this.mb.get(rowIndex + 1, colIndex + 1).getCard().getDir();
                treasure = this.mb.get(rowIndex + 1, colIndex + 1).getCard().getTreasure();

                layeredPane = new JLayeredPane();
                int number = rowIndex * this.size +colIndex;
                layeredPane.setName("P" + number);

                switch (type) {
                    case "I":
                        tmpDir = 0 + tmpDir;
                        c = new JLabel(images[tmpDir]);
                        break;
                    case "L":
                        tmpDir = 2 + tmpDir;
                        c = new JLabel(images[tmpDir]);
                        break;
                    case "T":
                        tmpDir = 6 + tmpDir;
                        c = new JLabel(images[tmpDir]);
                        break;
                    default:
                        c = new JLabel(images[0]);
                        break;
                }

                if (treasure != null) {
                    JLabel t = new JLabel(tImages[treasure.getTCcode()]);
                    t.setBounds(8, 8, imageSize-16, imageSize-16);
                    t.setVisible(true);
                    layeredPane.add(t, 2);
                }

                c.setName("L" + number);
                c.setBounds(0, 0, imageSize, imageSize);
                c.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        JLabel clicked = (JLabel) e.getSource();
                        String name = clicked.getName().substring(1);
                        
                        int index = Integer.parseInt(name);                                
                                
                        doAction(index);
                    }
                });
                c.setVisible(true);

                layeredPane.add(c, 1);
                layeredPane.setVisible(true);

                jPanel1.add(layeredPane);
            }
        }

        // hraci
        JLayeredPane foundLP;

        for (int i = 0; i < this.noP; i++) {
            c = new JLabel(pImages[i]);
            c.setName("RP" + i);
                
            c.setBounds(0, 0, 16, 16);
            c.setVisible(true);

            int pos = 0;
            switch (i) {
                case 0:
                    pos = (this.mb.getPlayer(i).getRowCoord() - 1) * this.size + (this.mb.getPlayer(i).getColCoord() - 1);
                    c.setBounds(2, 2, 16, 16);
                    break;
                case 1:
                    pos = (this.mb.getPlayer(i).getRowCoord() - 1) * this.size + (this.mb.getPlayer(i).getColCoord() - 1);
                    c.setBounds(46, 46, 16, 16);
                    break;
                case 2:
                    pos = (this.mb.getPlayer(i).getRowCoord() - 1) * this.size + (this.mb.getPlayer(i).getColCoord() - 1);
                    c.setBounds(2, 46, 16, 16);
                    break;
                case 3:
                    pos = (this.mb.getPlayer(i).getRowCoord() - 1) * this.size + (this.mb.getPlayer(i).getColCoord() - 1);
                    c.setBounds(46, 2, 16, 16);
                    break;
            }
            foundLP = (JLayeredPane) jPanel1.getComponent(pos);

            foundLP.add(c, 3);
            foundLP.revalidate();
            foundLP.repaint();
        }
        
        // zobrazeni info o hracich a jejich cilech
        p1Done.setText("" + this.mb.getPlayer(0).getAcquired());
        p1Goal.setIcon(tImages[this.mb.getPlayer(0).getGoal().getTCcode()]);
        p1Icon.setIcon(pImages[0]);

        p2Done.setText("" + this.mb.getPlayer(1).getAcquired());
        p2Goal.setIcon(tImages[this.mb.getPlayer(1).getGoal().getTCcode()]);
        p2Icon.setIcon(pImages[1]);

        switch (this.noP) {
            case 4:
                p4Done.setText("" + this.mb.getPlayer(3).getAcquired());
                p4Goal.setIcon(tImages[this.mb.getPlayer(3).getGoal().getTCcode()]);
                p4Icon.setIcon(pImages[3]);
            case 3:
                p3Done.setText("" + this.mb.getPlayer(2).getAcquired());
                p3Goal.setIcon(tImages[this.mb.getPlayer(2).getGoal().getTCcode()]);
                p3Icon.setIcon(pImages[2]);
                break;
        }
        
        p1Goal.setVisible(false);
        p2Goal.setVisible(false);
        p3Goal.setVisible(false);
        p4Goal.setVisible(false);
        
        switch (this.actualP) {
            case 0:
                p1Goal.setVisible(true);
                break;
            case 1:
                p2Goal.setVisible(true);
                break;
            case 2:
                p3Goal.setVisible(true);
                break;
            case 3:
                p4Goal.setVisible(true);
                break;            
        }

        jPanel1.revalidate();
        jPanel1.repaint();

        pack();
        revalidate();
        repaint();
    }
    
    /**
     * Akce po kliknuti na pole hraciho planu
     * @param index Urcuje polohu pole na hracim planu
     */
    private void doAction(int index) {
        
        int dir = 0;
        
        int row = index / this.size + 1;
        int col = index % this.size + 1;
        
        // vlozeni volne karty
        if (this.actualM == 0) {
            ShiftAction act;
            if (this.mb.isReverseShift(row, col)) {
                return;
            }
            Boolean canMove = this.mb.shift(row, col);
            dir = this.mb.getFreeCard().getDir();
            if (!canMove) {
                return;
            }
            act = new ShiftAction(dir, row, col);
            mb.storeAction(act);
        
        // pohyb hrace
        } else if (this.actualM == 1) {
            int origRow = this.mb.getPlayer(this.actualP).getRowCoord();
            int origCol = this.mb.getPlayer(this.actualP).getColCoord();
            MoveAction act;
            
            Boolean canGo = this.mb.movePlayer(this.actualP, row, col);
            if (!canGo) {
                return;
            }
            //this.mb.movePlayer(this.actualP, origRow, origCol);
            act = new MoveAction(origRow, origCol);
            mb.storeMove(act);
            TreasureCard tr = this.mb.get(row, col).getCard().getTreasure();
            strTreasure treasure;
            if (tr != null){
                treasure = new strTreasure(tr, row, col);
            } else {
                treasure = new strTreasure(null, row, col);
            }
            int result = mb.checkAssignGoal(this.actualP);
            
                    
            if (result == 1) {
                // prirazeni nove
                this.mb.getPlayer(this.actualP).raiseAcquired();
                if (this.mb.getPlayer(this.actualP).getAcquired() >= noT/this.noP)                
                {
                    
                    int winnerNo = this.actualP + 1;
                    String winnerIs = "Hráč " + winnerNo;
                    winnerOfGame.setText(winnerIs);
                    egDialog.setVisible(true);
                }
                 
                this.mb.storeTreasure(treasure);
                
            } else if (result == 0) {
                
                this.mb.getPlayer(this.actualP).raiseAcquired();
                // konec, dosly karty,vyhrava s nejvetsim poctem
                int maxAcquired[] = new int[this.noP];
                maxAcquired[0] = this.mb.getPlayer(0).getAcquired();
                maxAcquired[1] = this.mb.getPlayer(1).getAcquired();
                switch(this.noP) {
                    case 4:
                        maxAcquired[3] = this.mb.getPlayer(3).getAcquired();
                    case 3:
                        maxAcquired[2] = this.mb.getPlayer(2).getAcquired();
                        break;
                }
                int maxValue = 0;
                int maxIndex = 0;
                for (int i = 0; i < this.noP; i++) {
                    if (maxAcquired[i] > maxValue) {
                        maxValue = maxAcquired[i];
                        maxIndex = i;
                    }
                }
                
                int winnerNo = maxIndex + 1;
                String winnerIs = "Hráč " + winnerNo;
                winnerOfGame.setText(winnerIs);
                egDialog.setVisible(true);
            } else {
                treasure = new strTreasure(null, row, col);
                this.mb.storeTreasure(treasure);
            }

            this.actualP = (this.actualP + 1) % this.noP;
        }
//        act = new ShiftAction(dir, origX, origY);
//        this.mb.storeAction(act);
        this.actualM = (this.actualM + 1) % 2;

        step();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ngDialog = new javax.swing.JDialog();
        ngPanel = new javax.swing.JPanel();
        playersLabel = new javax.swing.JLabel();
        playersBox = new javax.swing.JComboBox();
        sizeLabel = new javax.swing.JLabel();
        sizeBox = new javax.swing.JComboBox();
        treasureLabel = new javax.swing.JLabel();
        treasureBox = new javax.swing.JComboBox();
        okButton = new javax.swing.JButton();
        jMenuItem2 = new javax.swing.JMenuItem();
        egDialog = new javax.swing.JDialog();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        winnerOfGame = new javax.swing.JLabel();
        closeEGDialog = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        p1Panel = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        p1Done = new javax.swing.JLabel();
        p1Icon = new javax.swing.JLabel();
        p1Goal = new javax.swing.JLabel();
        p2Panel = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        p2Done = new javax.swing.JLabel();
        p2Icon = new javax.swing.JLabel();
        p2Goal = new javax.swing.JLabel();
        p3Panel = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        p3Done = new javax.swing.JLabel();
        p3Icon = new javax.swing.JLabel();
        p3Goal = new javax.swing.JLabel();
        p4Panel = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        p4Done = new javax.swing.JLabel();
        p4Icon = new javax.swing.JLabel();
        p4Goal = new javax.swing.JLabel();
        freeCard = new javax.swing.JPanel();
        gameInfo = new javax.swing.JPanel();
        playerOnMove1 = new javax.swing.JLabel();
        awaitedMove1 = new javax.swing.JLabel();
        playerOnMove2 = new javax.swing.JLabel();
        awaitedMove2 = new javax.swing.JLabel();
        turnLeftButton = new javax.swing.JButton();
        turnRightButton = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        File = new javax.swing.JMenu();
        NewGame = new javax.swing.JMenuItem();
        LoadGame = new javax.swing.JMenuItem();
        SaveGame = new javax.swing.JMenuItem();
        Exit = new javax.swing.JMenuItem();
        Edit = new javax.swing.JMenu();
        Undo = new javax.swing.JMenuItem();

        ngDialog.setTitle("Nová hra");
        ngDialog.setLocationRelativeTo(null);
        ngDialog.setMinimumSize(new java.awt.Dimension(210, 240));

        playersLabel.setText("Počet hráčů:");

        playersBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "2", "3", "4" }));

        sizeLabel.setText("Velikost pole:");

        sizeBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "5", "7", "9", "11" }));
        sizeBox.setSelectedIndex(1);

        treasureLabel.setText("Počet pokladů:");

        treasureBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "12", "24" }));

        javax.swing.GroupLayout ngPanelLayout = new javax.swing.GroupLayout(ngPanel);
        ngPanel.setLayout(ngPanelLayout);
        ngPanelLayout.setHorizontalGroup(
            ngPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ngPanelLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(ngPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(treasureLabel)
                    .addComponent(sizeLabel)
                    .addComponent(playersLabel))
                .addGap(18, 18, 18)
                .addGroup(ngPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ngPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(playersBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(sizeBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(treasureBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        ngPanelLayout.setVerticalGroup(
            ngPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ngPanelLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(ngPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(playersLabel)
                    .addComponent(playersBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(ngPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sizeLabel)
                    .addComponent(sizeBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ngPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(treasureLabel)
                    .addComponent(treasureBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        okButton.setText("Ok");
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout ngDialogLayout = new javax.swing.GroupLayout(ngDialog.getContentPane());
        ngDialog.getContentPane().setLayout(ngDialogLayout);
        ngDialogLayout.setHorizontalGroup(
            ngDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ngDialogLayout.createSequentialGroup()
                .addGroup(ngDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ngDialogLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(ngPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(ngDialogLayout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addComponent(okButton)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        ngDialogLayout.setVerticalGroup(
            ngDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ngDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ngPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(okButton)
                .addContainerGap(140, Short.MAX_VALUE))
        );

        ngDialog.getRootPane().setDefaultButton(okButton);

        jMenuItem2.setText("jMenuItem2");

        egDialog.setMinimumSize(new java.awt.Dimension(400, 300));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel3.setText("Konec hry!");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel4.setText("Vítěz:");

        winnerOfGame.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        winnerOfGame.setText("Hráč 0");

        closeEGDialog.setText("Ok");
        closeEGDialog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeEGDialogActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout egDialogLayout = new javax.swing.GroupLayout(egDialog.getContentPane());
        egDialog.getContentPane().setLayout(egDialogLayout);
        egDialogLayout.setHorizontalGroup(
            egDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, egDialogLayout.createSequentialGroup()
                .addContainerGap(137, Short.MAX_VALUE)
                .addGroup(egDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(winnerOfGame)
                    .addComponent(jLabel3))
                .addGap(135, 135, 135))
            .addGroup(egDialogLayout.createSequentialGroup()
                .addGroup(egDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(egDialogLayout.createSequentialGroup()
                        .addGap(165, 165, 165)
                        .addComponent(jLabel4))
                    .addGroup(egDialogLayout.createSequentialGroup()
                        .addGap(171, 171, 171)
                        .addComponent(closeEGDialog)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        egDialogLayout.setVerticalGroup(
            egDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(egDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addComponent(winnerOfGame)
                .addGap(62, 62, 62)
                .addComponent(closeEGDialog)
                .addContainerGap(73, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setFocusTraversalPolicyProvider(true);

        jPanel1.setBackground(new java.awt.Color(0, 153, 0));
        jPanel1.setPreferredSize(new java.awt.Dimension(400, 400));
        jPanel1.setLayout(new java.awt.GridLayout(4, 4, 1, 1));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Hráč 1");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Získáno:");

        p1Done.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        p1Done.setText("0");

        p1Icon.setPreferredSize(new java.awt.Dimension(16, 16));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(p1Done)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(p1Icon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1)
                    .addComponent(p1Icon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(p1Done))
                .addGap(0, 10, Short.MAX_VALUE))
        );

        p1Goal.setMaximumSize(new java.awt.Dimension(32, 32));
        p1Goal.setMinimumSize(new java.awt.Dimension(32, 32));
        p1Goal.setPreferredSize(new java.awt.Dimension(32, 32));

        javax.swing.GroupLayout p1PanelLayout = new javax.swing.GroupLayout(p1Panel);
        p1Panel.setLayout(p1PanelLayout);
        p1PanelLayout.setHorizontalGroup(
            p1PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p1PanelLayout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(p1Goal, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        p1PanelLayout.setVerticalGroup(
            p1PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p1PanelLayout.createSequentialGroup()
                .addGroup(p1PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(p1PanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(p1Goal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel19.setText("Hráč 2");

        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel20.setText("Získáno:");

        p2Done.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        p2Done.setText("0");

        p2Icon.setPreferredSize(new java.awt.Dimension(16, 16));

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(jLabel20)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(p2Done))
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(jLabel19)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(p2Icon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel19)
                    .addComponent(p2Icon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(p2Done))
                .addGap(0, 10, Short.MAX_VALUE))
        );

        p2Goal.setMaximumSize(new java.awt.Dimension(32, 32));
        p2Goal.setMinimumSize(new java.awt.Dimension(32, 32));
        p2Goal.setName(""); // NOI18N
        p2Goal.setPreferredSize(new java.awt.Dimension(32, 32));

        javax.swing.GroupLayout p2PanelLayout = new javax.swing.GroupLayout(p2Panel);
        p2Panel.setLayout(p2PanelLayout);
        p2PanelLayout.setHorizontalGroup(
            p2PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p2PanelLayout.createSequentialGroup()
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(p2Goal, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        p2PanelLayout.setVerticalGroup(
            p2PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p2PanelLayout.createSequentialGroup()
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 40, Short.MAX_VALUE))
            .addGroup(p2PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(p2Goal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel12.setText("Hráč 3");

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel13.setText("Získáno:");

        p3Done.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        p3Done.setText("0");

        p3Icon.setPreferredSize(new java.awt.Dimension(16, 16));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(p3Done))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(p3Icon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel12)
                    .addComponent(p3Icon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(p3Done))
                .addGap(0, 10, Short.MAX_VALUE))
        );

        p3Goal.setMaximumSize(new java.awt.Dimension(32, 32));
        p3Goal.setMinimumSize(new java.awt.Dimension(32, 32));
        p3Goal.setName(""); // NOI18N
        p3Goal.setPreferredSize(new java.awt.Dimension(32, 32));

        javax.swing.GroupLayout p3PanelLayout = new javax.swing.GroupLayout(p3Panel);
        p3Panel.setLayout(p3PanelLayout);
        p3PanelLayout.setHorizontalGroup(
            p3PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p3PanelLayout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(p3Goal, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        p3PanelLayout.setVerticalGroup(
            p3PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p3PanelLayout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 30, Short.MAX_VALUE))
            .addGroup(p3PanelLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(p3Goal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setText("Hráč 4");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setText("Získáno:");

        p4Done.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        p4Done.setText("0");

        p4Icon.setPreferredSize(new java.awt.Dimension(16, 16));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(p4Done))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(p4Icon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel8)
                    .addComponent(p4Icon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(p4Done))
                .addGap(0, 10, Short.MAX_VALUE))
        );

        p4Goal.setMaximumSize(new java.awt.Dimension(32, 32));
        p4Goal.setMinimumSize(new java.awt.Dimension(32, 32));
        p4Goal.setPreferredSize(new java.awt.Dimension(32, 32));

        javax.swing.GroupLayout p4PanelLayout = new javax.swing.GroupLayout(p4Panel);
        p4Panel.setLayout(p4PanelLayout);
        p4PanelLayout.setHorizontalGroup(
            p4PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p4PanelLayout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(p4Goal, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        p4PanelLayout.setVerticalGroup(
            p4PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p4PanelLayout.createSequentialGroup()
                .addGroup(p4PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(p4PanelLayout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addComponent(p4Goal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 40, Short.MAX_VALUE))
        );

        freeCard.setBackground(new java.awt.Color(0, 153, 0));
        freeCard.setLayout(new java.awt.GridLayout(1, 0));

        gameInfo.setMaximumSize(new java.awt.Dimension(230, 110));
        gameInfo.setMinimumSize(new java.awt.Dimension(230, 110));
        gameInfo.setPreferredSize(new java.awt.Dimension(230, 110));

        playerOnMove1.setText("Hráč na tahu:");

        awaitedMove1.setText("Očekávaný tah:");

        playerOnMove2.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        playerOnMove2.setText("Hráč 1");

        awaitedMove2.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        awaitedMove2.setText("Zahájení nové hry");

        javax.swing.GroupLayout gameInfoLayout = new javax.swing.GroupLayout(gameInfo);
        gameInfo.setLayout(gameInfoLayout);
        gameInfoLayout.setHorizontalGroup(
            gameInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(gameInfoLayout.createSequentialGroup()
                .addGroup(gameInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(awaitedMove1)
                    .addComponent(playerOnMove1)
                    .addComponent(awaitedMove2)
                    .addComponent(playerOnMove2))
                .addGap(0, 114, Short.MAX_VALUE))
        );
        gameInfoLayout.setVerticalGroup(
            gameInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(gameInfoLayout.createSequentialGroup()
                .addComponent(playerOnMove1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                .addComponent(playerOnMove2)
                .addGap(18, 18, 18)
                .addComponent(awaitedMove1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(awaitedMove2)
                .addContainerGap())
        );

        turnLeftButton.setText("Otočení kamene doleva");
        turnLeftButton.setEnabled(false);
        turnLeftButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TurnLeftAction(evt);
            }
        });

        turnRightButton.setText("Otočení kamene doprava");
        turnRightButton.setEnabled(false);
        turnRightButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TurnRightAction(evt);
            }
        });

        File.setText("File");

        NewGame.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        NewGame.setText("New Game");
        NewGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NewGameActionPerformed(evt);
            }
        });
        File.add(NewGame);

        LoadGame.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.CTRL_MASK));
        LoadGame.setText("Load Game");
        LoadGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LoadGameActionPerformed(evt);
            }
        });
        File.add(LoadGame);

        SaveGame.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        SaveGame.setText("Save Game");
        SaveGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SaveGameActionPerformed(evt);
            }
        });
        File.add(SaveGame);

        Exit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        Exit.setText("Exit");
        Exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ExitActionPerformed(evt);
            }
        });
        File.add(Exit);

        jMenuBar1.add(File);

        Edit.setText("Edit");

        Undo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.CTRL_MASK));
        Undo.setText("Undo");
        Undo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UndoActionPerformed(evt);
            }
        });
        Edit.add(Undo);

        jMenuBar1.add(Edit);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(p1Panel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(gameInfo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 235, Short.MAX_VALUE)
                    .addComponent(p3Panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(freeCard, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(turnLeftButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(turnRightButton)))
                    .addComponent(p4Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(p2Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(p4Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(turnLeftButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(turnRightButton))
                            .addComponent(freeCard, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(51, 51, 51)
                        .addComponent(p2Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(p1Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(43, 43, 43)
                        .addComponent(gameInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(p3Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13))
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Ukonceni aplikace (kliknuti na krizek)
     * @param evt Zdroj akce
     */
    private void ExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ExitActionPerformed
        // TODO add your handling code here:
        //System.exit(0);
        this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }//GEN-LAST:event_ExitActionPerformed
    
    /**
     * Tlacitko nova hra
     * @param evt Zdroj akce
     */
    private void NewGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NewGameActionPerformed
        ngDialog.setVisible(true);
    }//GEN-LAST:event_NewGameActionPerformed
    
    /**
     * Dialog nova hra - potvrzeni nove hry
     * @param evt Zdroj akce
     */
    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        this.size = Integer.parseInt(sizeBox.getSelectedItem().toString());
        this.noP = Integer.parseInt(playersBox.getSelectedItem().toString());
        this.noT = Integer.parseInt(treasureBox.getSelectedItem().toString());

        this.actualM = 0;
        this.actualP = 0;

        this.mb = MazeBoard.createMazeBoard(size);
        this.mb.newGame(this.noP, this.noT);

        step();

        p3Panel.setVisible(false);
        p4Panel.setVisible(false);
        switch (this.noP) {
            case 4:
                p4Panel.setVisible(true);
            case 3:
                p3Panel.setVisible(true);
                break;
        }

        ngDialog.setVisible(false);

        turnLeftButton.setEnabled(true);
        turnRightButton.setEnabled(true);

        pack();
        revalidate();
        repaint();
    }//GEN-LAST:event_okButtonActionPerformed

    /**
     * Otoceni volne karty doleva - tlačítko
     * @param evt Zdroj akce
     */
    private void TurnLeftAction(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TurnLeftAction
        if (this.mb != null) {
            this.mb.getFreeCard().turnRight(3);
            step();
        }
    }//GEN-LAST:event_TurnLeftAction
    
    /**
     * Otoceni volne karty doprava - tlacitko
     * @param evt Zdroj akce
     */
    private void TurnRightAction(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TurnRightAction
        if (this.mb != null) {
            this.mb.getFreeCard().turnRight(1);
            step();
        }
    }//GEN-LAST:event_TurnRightAction
    
    /**
     * Nacteni ulozene hry - tlacitko
     * @param evt Zdroj akce
     */
    private void LoadGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LoadGameActionPerformed
        if (this.mb == null) { //workaround
            this.mb = MazeBoard.createMazeBoard(7);
        }
        this.mb = this.mb.loadState("../examples/quicksave.ija");
        this.noP = this.mb.getPlayerNumber();
        this.noT = this.mb.getPackSize();
        this.size = this.mb.getSize();
        this.actualM = this.mb.getActualM();
        this.actualP = this.mb.getActualP();
        
        turnLeftButton.setEnabled(true);
        turnRightButton.setEnabled(true);
        
        p3Panel.setVisible(false);
        p4Panel.setVisible(false);
        switch (this.noP) {
            case 4:
                p4Panel.setVisible(true);
            case 3:
                p3Panel.setVisible(true);
                break;
        }
        
        step();
    }//GEN-LAST:event_LoadGameActionPerformed
    
    /**
     * Ulozeni hry - tlacitko
     * @param evt Zdroj akce
     */
    private void SaveGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SaveGameActionPerformed
        if (this.mb != null){
            this.mb.setActualM(actualM);
            this.mb.setActualP(actualP);
            this.mb.saveState("../examples/quicksave.ija");
        }
    }//GEN-LAST:event_SaveGameActionPerformed
    
    /**
     * Zavreni dialogu
     * @param evt Zdroj akce
     */
    private void closeEGDialogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeEGDialogActionPerformed
        egDialog.setVisible(false);        
        ngDialog.setVisible(true);
    }//GEN-LAST:event_closeEGDialogActionPerformed
    
    /**
     * Krok zpet - tlacitko
     * @param evt Zdroj akce
     */
    private void UndoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UndoActionPerformed
        if (this.mb == null) {
            return;
        }
        if (actualM == 1) {
            ShiftAction act = this.mb.getAction();
            if (act != null) {
                this.mb.undoShift(act.getRow(), act.getCol(), act.getDir());
            } else {
                return;
            }
        } else if (actualM == 0) {
            MoveAction mov = this.mb.getMove();
            if (mov != null) {
                if (this.actualP == 0){
                    this.actualP = this.noP - 1;
                } else {
                    this.actualP -= 1;
                }
                this.mb.movePlayer(actualP, mov.getRow(), mov.getCol());
                strTreasure treasure = this.mb.getTreasure();
                if (treasure != null && treasure.getCard() != null){
                    Player player = this.mb.getPlayer(actualP);
                    player.decreaseAcquired();
                    mb.pushPackCard(player.getGoal());
                    mb.putTreasureCard(treasure.getCard(), treasure.getRow(), treasure.getCol());
                    player.setNewGoal(treasure.getCard());
                    
                    
                }

            } else {
                return;
            }
            
        }        
        this.actualM = (this.actualM + 1) % 2;
        step();
    }//GEN-LAST:event_UndoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu Edit;
    private javax.swing.JMenuItem Exit;
    private javax.swing.JMenu File;
    private javax.swing.JMenuItem LoadGame;
    private javax.swing.JMenuItem NewGame;
    private javax.swing.JMenuItem SaveGame;
    private javax.swing.JMenuItem Undo;
    private javax.swing.JLabel awaitedMove1;
    private javax.swing.JLabel awaitedMove2;
    private javax.swing.JButton closeEGDialog;
    private javax.swing.JDialog egDialog;
    private javax.swing.JPanel freeCard;
    private javax.swing.JPanel gameInfo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JDialog ngDialog;
    private javax.swing.JPanel ngPanel;
    private javax.swing.JButton okButton;
    private javax.swing.JLabel p1Done;
    private javax.swing.JLabel p1Goal;
    private javax.swing.JLabel p1Icon;
    private javax.swing.JPanel p1Panel;
    private javax.swing.JLabel p2Done;
    private javax.swing.JLabel p2Goal;
    private javax.swing.JLabel p2Icon;
    private javax.swing.JPanel p2Panel;
    private javax.swing.JLabel p3Done;
    private javax.swing.JLabel p3Goal;
    private javax.swing.JLabel p3Icon;
    private javax.swing.JPanel p3Panel;
    private javax.swing.JLabel p4Done;
    private javax.swing.JLabel p4Goal;
    private javax.swing.JLabel p4Icon;
    private javax.swing.JPanel p4Panel;
    private javax.swing.JLabel playerOnMove1;
    private javax.swing.JLabel playerOnMove2;
    private javax.swing.JComboBox playersBox;
    private javax.swing.JLabel playersLabel;
    private javax.swing.JComboBox sizeBox;
    private javax.swing.JLabel sizeLabel;
    private javax.swing.JComboBox treasureBox;
    private javax.swing.JLabel treasureLabel;
    private javax.swing.JButton turnLeftButton;
    private javax.swing.JButton turnRightButton;
    private javax.swing.JLabel winnerOfGame;
    // End of variables declaration//GEN-END:variables
    
    
    /**
     * Override metody run
     */
    @Override
    public void run() {
        this.setVisible(true);        
        repaint();

    }
}
