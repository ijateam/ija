/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.project.game;

import ija.project.board.MazeBoard;
import ija.project.board.MazeCard;
import ija.project.treasure.*;
import java.awt.Color;
import java.io.Serializable;

/**
 *
 * @author Lukas Velecky
 */
public class Player implements Serializable {

    private int number;

    private int colCoords;
    private int rowCoords;

    private TreasureCard goal;
    private int acquired;

    private Color color;
    
    /**
     * Vytvoreni hrace
     * @param p cislo hrace
     */
    public Player(int p) {
        this.color = chooseColor(p);
        this.number = p;
        this.acquired = 0;
    }
    
    /**
     * Vybrani barvy hrace na zaklade parametru p
     * @param p Cislo hrace
     * @return Navratova barva
     */
    private Color chooseColor(int p) {
        Color[] colors = new Color[4];
        colors[0] = Color.red;
        colors[1] = Color.blue;
        colors[2] = Color.yellow;
        colors[3] = Color.green;
        return colors[p];
    }
    
    /**
     * @return Vraci barvu hrace
     */
    public Color getColor() {
        return this.color;
    }

    /**
     * @return Vraci cíl hrace
     */
    public TreasureCard getGoal() {
        return this.goal;
    }

    /**
     * Priradi hraci novy ukol
     * @param T Nova karta pokladu
     */
    public void setNewGoal(TreasureCard T) {
        this.goal = T;
    }

    /**
     * @return Vraci souradnice hrace (column)
     */
    public int getColCoord() {
        return this.colCoords;
    }

    /**
     * @return Vraci souradnice hrace (row)
     */
    public int getRowCoord() {
        return this.rowCoords;
    }

    /**
     * Nastavuje souradnice hraci
     * @param row Souradnice radku
     * @param col Souradnice sloupce
     */
    public void setCoords(int row, int col) {
        this.colCoords = col;
        this.rowCoords = row;
    }

    /**
     * Pohyb hrace po desce
     * @param row Nove souradnice radku
     * @param col Nove souradnice sloupce
     * @param mb Hraci deska
     * @return Kontrola, zdali probehla operace v poradku
     */
    public boolean move(int row, int col, MazeBoard mb) {

        assert (col > 0 && col <= mb.getSize()) : "Move out of bounds x";
        assert (row > 0 && row <= mb.getSize()) : "Move out of bounds y";

        //nula kroku
        if (this.colCoords == col && this.rowCoords == row){
            return true;
        }
        
        //po radku
        if (this.colCoords != col && this.rowCoords == row) {
            //doleva
            if (this.colCoords > col) {
                for (int i = this.colCoords; i > col; i--) {
                    if (!mb.get(row, i).getCard().canGo(MazeCard.CANGO.LEFT)
                            || !mb.get(row, i - 1).getCard().canGo(MazeCard.CANGO.RIGHT)) {
                        return false;
                    }
                }
            } else { //doprava
                for (int i = this.colCoords; i < col; i++) {
                    if (!mb.get(row, i).getCard().canGo(MazeCard.CANGO.RIGHT)
                            || !mb.get(row, i + 1).getCard().canGo(MazeCard.CANGO.LEFT)) {
                        return false;
                    }
                }
            }
            this.setCoords(row, col);
            return true;
        }
        //po sloupci
        if (this.colCoords == col && this.rowCoords != row) {
            //nahoru
            if (this.colCoords > row) {
                for (int i = this.rowCoords; i > row; i--) {
                    if (!mb.get(i, col).getCard().canGo(MazeCard.CANGO.UP)
                            || !mb.get(i-1, col).getCard().canGo(MazeCard.CANGO.DOWN)) {
                        return false;
                    }
                }
            } else { //dolu
                for (int i = this.rowCoords; i < row; i++) {
                    if (!mb.get(i, col).getCard().canGo(MazeCard.CANGO.DOWN)
                            || !mb.get(i+1, col).getCard().canGo(MazeCard.CANGO.UP)) {
                        return false;
                    }
                }
            }
            this.setCoords(row, col);
            return true;
        }
        return false;
    }

    /**
     * Posune hrace o col sloupcu
     * @param col Souradnice sloupce
     */
    public void addColCoord(int col) {
        this.colCoords += col;
    }

    /**
     * Posune hrace o row radku
     * @param row Souradnice radku
     */
    public void addRowCoord(int row) {
        this.rowCoords += row;
    }
    
    /**
     * @return Vraci pocet ziskanych pokladu
     */
    public int getAcquired() {
        return this.acquired;
    }

    /**
     * Zvysi pocet ziskanych pokladu
     */
    public void raiseAcquired() {
        this.acquired += 1;
    }
    
    /**
     * Snizi pocet ziskanych pokladu
     */
    public void decreaseAcquired() {
        this.acquired -= 1;
    }

    /**
     * @return Vraci cislo hrace
     */
    public int getNumber() {
        return number;
    }

}
