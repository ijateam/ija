/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.project.game;

import java.io.Serializable;

/**
 *
 * @author Martin Zemek, xzemek04
 */
public class ShiftAction implements Serializable{
    private final int dir;
    private final int row;
    private final int col;

    /**
     * Vytvori akci posunu karty pro ulozeni
     * @param dir natoceni
     * @param row radek
     * @param col sloupec
     */
    public ShiftAction(int dir, int row, int col) {
        this.dir = dir;
        this.row = row;
        this.col = col;
    }

    /**
     * Vrati puvodni natoceni karty
     * @return natoceni
     */
    public int getDir() {
        return dir;
    }

    /**
     * Vrati radek
     * @return radek
     */
    public int getRow() {
        return row;
    }

    /**
     * Vrati sloupec
     * @return sloupec
     */
    public int getCol() {
        return col;
    }
    
    
    
}
