/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.project.board;

import java.io.Serializable;

/**
 *
 * @author Martin Zemek, xzemek04
 */
public class MazeField implements Serializable{

    private final int row;
    private final int col;
    private MazeCard card;

    /**
     * Konstruktor
     *
     * @param row radek
     * @param col sloupec
     */
    public MazeField(int row, int col) {
        this.row = row;
        this.col = col;
        this.card = null;
    }


    /**
     * Vraci cislo radku
     *
     * @return radek
     */
    public int row() {
        return row;
    }

    /**
     * Vraci cislo sloupce
     *
     * @return sloupec
     */
    public int col() {
        return col;
    }

    /**
     * Vraci kartu
     *
     * @return karta
     */
    public MazeCard getCard() {
        return card;
    }

    /**
     * Vlozi kartu
     *
     * @param c karta
     */
    public void putCard(MazeCard c) {
        this.card = c;
    }

    
}
