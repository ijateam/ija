/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.project.board;

import ija.project.game.MoveAction;
import ija.project.game.ShiftAction;
import ija.project.game.Player;
import ija.project.game.strTreasure;
import ija.project.treasure.CardPack;
import ija.project.treasure.Treasure;
import ija.project.treasure.TreasureCard;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.EmptyStackException;
import java.util.NoSuchElementException;
import java.util.Random;

/**
 *
 * @author Martin Zemek, xzemek04
 */
public class MazeBoard implements Serializable {

    private final MazeField[][] fields;
    private MazeCard freeCard;
    private Player[] players;
    private CardPack pack;
    private final Deque<ShiftAction> shifts;
    private final Deque<MoveAction> moves;
    private final Deque<strTreasure> treasures;
    private int actualM;
    private int actualP;
    
    /**
     * Konstruktor, vytvori desku o n X n rozmerech
     *
     * @param n rozmery desky
     */
    private MazeBoard(int n) {
        this.players = null;
        this.freeCard = null;
        this.shifts = new ArrayDeque<>();
        this.moves = new ArrayDeque<>();
        this.treasures = new ArrayDeque<>();
        assert (n >= 5 && n <= 11 && n % 2 == 1) : "Invalid MazeBoard size, correct is odd <7-11>";
        this.fields = new MazeField[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                this.fields[i][j] = new MazeField(i + 1, j + 1);
            }
        }
        this.pack = null;
    }

    /**
     * Vytvoreni ctvercoveho hraciho pole o velikosti n
     *
     * @param n rozmer
     * @return hraci pole
     */
    public static MazeBoard createMazeBoard(int n) {
        MazeBoard board = new MazeBoard(n);
        return board;
    }

    /**
     * Vrati velikost hraci desky
     * @return velikost hraci desky
     */
    public int getSize() {
        return this.fields.length;
    }

    /**
     * Vrati balicek s hracima kartama
     * @return balicek karet
     */
    public CardPack getPack() {
        return pack;
    }

    /**
     * Ulozi akci posunu kamene na zasobnik
     * @param item akce posunu kamene
     */
    public void storeAction(ShiftAction item){
        shifts.push(item);
    }
    
    /**
     * Vrati akci posunu kamene ze zasobniku
     * @return akce posunu kamene
     */
    public ShiftAction getAction(){
        ShiftAction act;
        try {
        act = shifts.pop();
        } catch (EmptyStackException | NoSuchElementException e){
            return null;
        }
        return act;
    }
    
    /**
     * Ulozi poklad na zasobnik ziskanych pokladu
     * @param item akce pokladu
     */
    public void storeTreasure(strTreasure item){
        treasures.push(item);
    }
    
    /**
     * Vrati ulozeny poklad ze zasobniku ziskanych pokladu
     * @return akce pokladu
     */
    public strTreasure getTreasure(){
        strTreasure act;
        try {
        act = treasures.pop();
        } catch (EmptyStackException | NoSuchElementException e){
            return null;
        }
        return act;
    }
    
    /**
     * Ulozi pohyb kamene na zasobnik
     * @param item akce pohybu
     */
    public void storeMove (MoveAction item){
        moves.push(item);
    }
    
    /**
     * Vrati pohyb kamene ze zasobniku
     * @return akce pohybu
     */
    public MoveAction getMove(){
        MoveAction act;
        try {
        act = moves.pop();
        } catch (EmptyStackException | NoSuchElementException e){
            return null;
        }
        return act;
    }
    
    /**
     * Vazeny prumer.
     * kazdy parametr je pocet procent
     * @param l prvni
     * @param i druhy
     * @param t treti
     * @return nahodny
     */
    private int weightedRandom(int l, int i, int t) {
        Random rand = new Random();
        int value;
        value = rand.nextInt(l + i + t);
        if (value < l) {
            return 0;
        } else if (value < l + i) {
            return 1;
        } else if (value < l + i + t) {
            return 2;
        } else {
            //should not reach here
            assert (false) : "Random is wrong";
            return -1;
        }
    }

    /**
     * Vytvori novou hru
     * @param pocetHracu pocet hracu
     * @param pocetTreasure pocet pokladu
     */
    public void newGame(int pocetHracu, int pocetTreasure) {
        Random rand = new Random();
        int newType;


        String[] types = {"L", "I", "T"};
        for (MazeField[] field : fields) {
            for (MazeField field1 : field) {
                MazeCard m = null;

                //kazdy lichy na okraji
                //horni okraj
                if ((field1.row() == 1) && (field1.col() % 2) == 1) {
                    m = MazeCard.create("T");
                    m.turnRight(2);
                }
                //dolni okraj
                if ((field1.row() == fields.length) && (field1.col() % 2) == 1) {
                    m = MazeCard.create("T");
                }
                //levy okraj
                if ((field1.col() == 1) && (field1.row() % 2) == 1) {
                    m = MazeCard.create("T");
                    m.turnRight(1);
                }
                //pravy okraj
                if ((field1.col() == fields.length) && (field1.row() % 2) == 1) {
                    m = MazeCard.create("T");
                    m.turnRight(3);
                }

                //Rohy
                if (field1.row() == 1 && field1.col() == 1) {
                    m = MazeCard.create("L");
                    m.turnRight(2);
                } else if ((field1.row() == 1
                        && field1.col() == fields.length)) {
                    m = MazeCard.create("L");
                    m.turnRight(3);
                } else if ((field1.row() == field.length
                        && field1.col() == 1)) {
                    m = MazeCard.create("L");
                    m.turnRight(1);
                } else if ((field1.row() == field.length
                        && field1.col() == fields.length)) {
                    m = MazeCard.create("L");
                }
                if ((field1.col() % 2) == 1 && (field1.row() % 2) == 1 && m == null) {
                    m = MazeCard.create("T");
                    m.turnRight((rand.nextInt(3)));
                }

                if (m == null) {
                    int share = fields.length * field.length / types.length;
                    newType = weightedRandom(share - 4, share, share - (fields.length / 2 * 4));
                    m = MazeCard.create(types[newType]);
                    m.turnRight(rand.nextInt(3));

                }
                m.putTreasure(null);
                field1.putCard(m);
            }
        }
        freeCard = MazeCard.create(types[rand.nextInt(3)]);
        freeCard.turnRight(rand.nextInt(3));
        Treasure.createSet();
        MazeField mf;
        for (int i = 0; i < pocetTreasure; i++) {
            int x;
            int y;
            do {
                x = rand.nextInt(fields.length) + 1;
                y = rand.nextInt(fields.length) + 1;
                mf = this.get(x, y);
            } while (mf.getCard().getTreasure() != null);
            TreasureCard tr = new TreasureCard(Treasure.getTreasure(i));
            mf.getCard().putTreasure(tr);
        }
        this.pack = new CardPack(pocetTreasure, pocetTreasure);
        this.pack.shuffle();
        
        //vytvoreni hracu;
        this.players = new Player[pocetHracu];
        for (int i = 0; i < pocetHracu; i++) {
            this.players[i] = new Player(i);
            this.players[i].setNewGoal(pack.popCard());
            switch (i) {
                case 0:
                    this.players[i].setCoords(1, 1);
                    break;
                case 1:
                    this.players[i].setCoords(fields.length, fields.length);
                    break;
                case 2:
                    this.players[i].setCoords(fields.length, 1);
                    break;
                case 3:
                    this.players[i].setCoords(1, fields.length);
                    break;
                default:
                    System.err.println("Nepodporovaný počet hráčů");
            }
        }
    }

    /**
     * Vrati herni policko na radku r a sloupci c
     *
     * @param r radek
     * @param c sloupec
     * @return mazeField z fields[r,c]
     */
    public MazeField get(int r, int c) {
        if ((r < 1 || r > fields.length) || (c < 1 || c > fields[r - 1].length)) {
            return null;
        }
        return fields[r - 1][c - 1];
    }

    /**
     * Vrati prazdnou kartu
     *
     * @return MazeCard
     */
    public MazeCard getFreeCard() {
        return freeCard;
    }

    /**
     * Posune hrace, pri posunu kamenu
     * @param row radek
     * @param col sloupec
     * @param movCol posunuti v sloupci
     * @param movRow posunuti v radku
     */
    private void shiftPlayers(int row, int col, int movCol, int movRow) {
        if (this.players == null) {
            return;
        }
        for (Player player : players) {
            int colPlayer = player.getColCoord();
            int rowPlayer = player.getRowCoord();
            if (movCol != 0 && movRow == 0 && rowPlayer == row ) {
                //pres okraj
                if (movCol < 0 && colPlayer == 1) {
                    player.setCoords(rowPlayer, this.fields.length);
                } else if (movCol > 0 && colPlayer == this.fields.length) {
                    player.setCoords(rowPlayer, 1);
                } else {
                    player.addColCoord(movCol);
                }

            } else if (movCol == 0 && movRow != 0 && colPlayer == col) {
                if (movRow < 0 && rowPlayer == 1) {
                    player.setCoords(this.fields.length, colPlayer);
                } else if (movRow > 0 && rowPlayer == this.fields.length) {
                    player.setCoords(1, colPlayer);
                } else {
                    player.addRowCoord(movRow);
                }
            }
        }
    }

    /**
     * Vlozi prazdnou kartu do herni desky
     *
     * @param row radek
     * @param col sloupec
     * @return boolean
     */
    public boolean shift(int row, int col) {
        MazeCard tmp;
        int rowIndex = row - 1;
        int colIndex = col - 1;
        int n = fields.length - 1;
        //posouvani dolu
        if ((row == 1) && (col % 2) == 0) {
            shiftPlayers(row, col, 0, +1);
            tmp = fields[n][colIndex].getCard();
            for (int i = n; i > 0; i--) {
                fields[i][colIndex].putCard(fields[i - 1][colIndex].getCard());
            }
            fields[0][colIndex].putCard(freeCard);
            freeCard = tmp;
            return true;
        }
        //posouvani nahoru
        if ((row == fields.length) && (col % 2) == 0) {
            shiftPlayers(row, col, 0, -1);
            tmp = fields[0][colIndex].getCard();
            for (int i = 0; i < n; i++) {
                fields[i][colIndex].putCard(fields[i + 1][colIndex].getCard());
            }
            fields[n][colIndex].putCard(freeCard);
            freeCard = tmp;
            return true;
        }
        //posouvani doprava
        if ((col == 1) && (row % 2) == 0) {
            shiftPlayers(row, col, +1, 0);
            tmp = fields[rowIndex][n].getCard();
            for (int i = n; i > 0; i--) {
                fields[rowIndex][i].putCard(fields[rowIndex][i - 1].getCard());
            }
            fields[rowIndex][0].putCard(freeCard);
            freeCard = tmp;
            return true;
        }
        //posouvani doleva
        if ((col == fields.length) && (row % 2) == 0) {
            shiftPlayers(row, col, -1, 0);
            tmp = fields[rowIndex][0].getCard();
            for (int i = 0; i < n; i++) {
                fields[rowIndex][i].putCard(fields[rowIndex][i + 1].getCard());
            }
            fields[rowIndex][n].putCard(freeCard);
            freeCard = tmp;
            return true;
        }
        return false;
    }
    
    /**
     * Reverzni operace k operaci shift()
     * Navic lze otocit vkladanou kartu
     * @param row radek
     * @param col sloupec
     * @param dir natoceni
     * @return boolean
     */
    public boolean undoShift(int row, int col, int dir) {
        MazeCard tmp;
        int rowIndex = row - 1;
        int colIndex = col - 1;
        int n = fields.length - 1;
        
        while(freeCard.getDir() != dir){
            freeCard.turnRight(1);
        }
        //posouvani dolu
        if ((row == 1) && (col % 2) == 0) {
            
            shiftPlayers(row, col, 0, -1);
            tmp = fields[0][colIndex].getCard();
            for (int i = 0; i < n; i++) {
                fields[i][colIndex].putCard(fields[i + 1][colIndex].getCard());
            }
            fields[n][colIndex].putCard(freeCard);
            freeCard = tmp;
            return true;
        }
        //posouvani nahoru
        if ((row == fields.length) && (col % 2) == 0) {
            shiftPlayers(row, col, 0, +1);
            tmp = fields[n][colIndex].getCard();
            for (int i = n; i > 0; i--) {
                fields[i][colIndex].putCard(fields[i - 1][colIndex].getCard());
            }
            fields[0][colIndex].putCard(freeCard);
            freeCard = tmp;
            return true;
        }
        //posouvani doprava
        if ((col == 1) && (row % 2) == 0) {
            shiftPlayers(row, col, -1, 0);
            tmp = fields[rowIndex][0].getCard();
            for (int i = 0; i < n; i++) {
                fields[rowIndex][i].putCard(fields[rowIndex][i + 1].getCard());
            }
            fields[rowIndex][n].putCard(freeCard);
            freeCard = tmp;
            return true;
        }
        //posouvani doleva
        if ((col == fields.length) && (row % 2) == 0) {
            shiftPlayers(row, col, +1, 0);
            tmp = fields[rowIndex][n].getCard();
            for (int i = n; i > 0; i--) {
                fields[rowIndex][i].putCard(fields[rowIndex][i - 1].getCard());
            }
            fields[rowIndex][0].putCard(freeCard);
            freeCard = tmp;
            return true;
        }
        return false;
    }

    /**
     * Posune hracem na urcenou pozici
     * @param pNumber cislo hrace
     * @param row radek
     * @param col sloupec
     * @return boolean
     */
    public boolean movePlayer(int pNumber, int row, int col) {
        return this.players[pNumber].move(row, col, this);
    }
    
    /**
     * Zkontroluje jestli hrac nedosahl cile a pripadne mu prideli novy
     * @param playerN cislo hrace
     * @return 0 pokud dojdou karty v balicku
     *  1 pokud se provedlo prirazeni
     *  2 pokud se nic nestalo
    */
    public int checkAssignGoal(int playerN){
        Player player = players[playerN];
        //nefunguje
        TreasureCard treasure = this.get(player.getRowCoord(), player.getColCoord()).getCard().getTreasure();
        if (player.getGoal().equals(treasure)){
            treasure = pack.popCard();
            if (treasure == null){
                return 0;
            }
            player.setNewGoal(treasure);
            this.get(player.getRowCoord(), player.getColCoord()).getCard().putTreasure(null);
            return 1;
        } 
        return 2;
    }
    
    /**
     * Vrati kartu pokladu do balicku
     * @param item karta pokladu
     */
    public void pushPackCard(TreasureCard item){
        pack.pushCard(item);
    }
    
    /**
     * Vlozi kartu pokladu na hraci desku
     * @param item karta pokladu
     * @param row radek
     * @param col sloupec
     */
    public void putTreasureCard(TreasureCard item, int row, int col){
        this.get(row, col).getCard().putTreasure(item);
    }
    
    /**
     * Vrati hrace podle cisla
     * @param pNumber cislo hrace
     * @return hrac
     */
    public Player getPlayer(int pNumber) {
        return this.players[pNumber];
    }

    /**
     * vytiskne hraci plochu(textove)
     * @deprecated 
     */
    public void print() {
        MazeCard mc;
        for (MazeField[] field : this.fields) {
            for (int j = 0; j < this.fields.length; j++) {
                mc = field[j].getCard();
                if (mc == null) {
                    System.out.print("-\t");
                } else {
                    System.out.print(mc.getType());
                    if (mc.getTreasure() != null) {
                        System.out.print(mc.getTreasure().getTCcode());
                    }
                    if (this.players != null) {
                        for (Player player : players) {
                            if (player.getColCoord() == field[j].col()
                                    && player.getRowCoord() == field[j].row()) {
                                System.out.print("p" + player.getNumber());
                            }
                        }
                    }
                    System.out.print("\t");
                }
            }
            System.out.println();
        }
        System.out.println();
        if (this.freeCard != null) {
            System.out.print("Volná karta: " + this.freeCard.getType());
            if (this.freeCard.getTreasure() != null) {
                System.out.println(this.freeCard.getTreasure().getTCcode());
            } else {
                System.out.println();
            }
        }

    }
    
    /**
     * Ulozi hru do souboru
     * @param filename nazev souboru
     * @return boolean
     */
    public boolean saveState(String filename) {
        try {
        MazeBoard m = this;
        FileOutputStream myFileOutputStream = new FileOutputStream(new File(filename));
        ObjectOutputStream myObjectOutputStream = new ObjectOutputStream(myFileOutputStream);
        myObjectOutputStream.reset();
        myObjectOutputStream.writeObject(m);
        myFileOutputStream.close();
        myObjectOutputStream.close();
        } catch (IOException e){
            System.err.println("Chyba prace se souborem " + filename);
            return false;
        }
        return true;
    }
    
    /**
     * Nacte hru ze souboru
     * @param filename nazev souboru
     * @return nactena hraci deska
     */
    public MazeBoard loadState(String filename) {
        try {
            FileInputStream myFileInputStream = new FileInputStream(filename);
            ObjectInputStream myObjectInputStream = new ObjectInputStream(myFileInputStream);
            MazeBoard loaded;
            loaded = (MazeBoard) myObjectInputStream.readObject();
            myFileInputStream.close();
            myObjectInputStream.close();
            return loaded;
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Soubor " + filename + " nebyl nalezen nebo nespravny format");
            return null;
        }
    }
    
    /**
     * Vrati cislo hrace
     * @return cislo hrace
     */
    public int getPlayerNumber() {
        return this.players.length;
    }
    
    /**
     * Vrati velikost balicku karet pokladu
     * @return velikost balicku
     */
    public int getPackSize(){
        return this.pack.getSize();
    }
    
    /**
     * vrati aktualni krok (pouzito pro nacitani hry)
     * @return aktualni krok
     */
    public int getActualM() {
        return actualM;
    }

    /**
     * nastavi aktualni krok (pouzito pro nacitani hry)
     * @param actualM aktualni krok
     */
    public void setActualM(int actualM) {
        this.actualM = actualM;
    }

    /**
     * Vrati aktualniho hrace (pouzito pro nacitani hry)
     * @return aktualni hrac
     */
    public int getActualP() {
        return actualP;
    }

    /**
     * Nastavi aktualniho hrace (pouzito pro nacitani hry)
     * @param actualP aktualni hrac
     */
    public void setActualP(int actualP) {
        this.actualP = actualP;
    }
    
    /**
     * Zkontroluje zda jde o reverzni posun kamenu
     * @param row radek
     * @param col sloupec
     * @return boolean
     */
    public boolean isReverseShift(int row, int col){
        ShiftAction act =  this.shifts.peek();
        if (act == null) {
            return false;
        }
        int lastRow = act.getRow();
        int lastCol = act.getCol();
        //dolu
        if ((row == 1) && (col % 2) == 0) {
            if (lastRow == fields.length && lastCol == col){
                return true;
            }
        }
        //posouvani nahoru
        if ((row == fields.length) && (col % 2) == 0) {
            if (lastRow == 1 && lastCol == col){
                return true;
            }
        }
        //posouvani doprava
        if ((col == 1) && (row % 2) == 0) {
            if (lastRow == row && lastCol == fields.length){
                return true;
            }
        }
        //posouvani doleva
        if ((col == fields.length) && (row % 2) == 0) {
            if (lastRow == row && lastCol == 1){
                return true;
            }
        }
        return false;
    }
}
