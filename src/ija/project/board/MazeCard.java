/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.project.board;

import ija.project.treasure.TreasureCard;
import java.io.Serializable;

/**
 *
 * @author Martin Zemek, xzemek04
 */
public class MazeCard implements Serializable {

    private final boolean[] directions = new boolean[4];
    private final String type;
    private int dir;
    private TreasureCard treasure;
    
    /**
     * Smery kudy muze jit.
     */
    public static enum CANGO {

        LEFT(0), UP(1), RIGHT(2), DOWN(3);
        private final int value;

        /**
         * konstrukor
         * @param value hodnota 
         */
        private CANGO(int value) {
            this.value = value;
        }

        /**
         * Vrati integer podle typu
         *
         * @return cislo typu
         */
        public int getValue() {
            return value;
        }

    }

    /**
     * Konstruktor, nastavi kartu na pozadovany typu
     *
     * @param type typ karty
     */
    private MazeCard(String type) {
        this.type = type;
        this.treasure = null;
        this.dir = 0;
    }

    /**
     * Vytvori kartu a nastavi mozne smery
     * <ul>
     * <li>L - ukazuje doleva a nahoru</li>
     * <li>I - ukazuje doleva a doprava</li>
     * <li>T - ukazuje doleva, doprava a nahoru</li>
     * </ul>
     *
     * @param type moznosti L, I, T
     * @return vytvorena karta
     */
    public static MazeCard create(String type) {
        MazeCard m = new MazeCard(type);
        switch (type) {
            case "L":
                m.directions[CANGO.LEFT.getValue()] = true;
                m.directions[CANGO.UP.getValue()] = true;
                m.directions[CANGO.RIGHT.getValue()] = false;
                m.directions[CANGO.DOWN.getValue()] = false;
                break;
            case "I":
                m.directions[CANGO.LEFT.getValue()] = true;
                m.directions[CANGO.UP.getValue()] = false;
                m.directions[CANGO.RIGHT.getValue()] = true;
                m.directions[CANGO.DOWN.getValue()] = false;
                break;
            case "T":
                m.directions[CANGO.LEFT.getValue()] = true;
                m.directions[CANGO.UP.getValue()] = true;
                m.directions[CANGO.RIGHT.getValue()] = true;
                m.directions[CANGO.DOWN.getValue()] = false;
                break;
            default:
                throw new IllegalArgumentException();
        }
        return m;
    }

    /**
     * Vrati, jestli se muze jit timto smerem
     *
     * @param dir smer
     * @return boolean
     */
    public boolean canGo(MazeCard.CANGO dir) {
        return this.directions[dir.getValue()];
    }

    /**
     * Otoci kamenem doprava o n
     * @param n pocet otoceni
     */
    public void turnRight(int n) {
        for (int i = 0; i < n ; i++) {
            boolean down = this.directions[CANGO.DOWN.getValue()];
            this.directions[CANGO.DOWN.getValue()] = this.directions[CANGO.RIGHT.getValue()];
            this.directions[CANGO.RIGHT.getValue()] = this.directions[CANGO.UP.getValue()];
            this.directions[CANGO.UP.getValue()] = this.directions[CANGO.LEFT.getValue()];
            this.directions[CANGO.LEFT.getValue()] = down;
        }
        
        switch (type) {
            case "I":
                this.dir = (this.dir + n) % 2;
                break;
            default:
                this.dir = (this.dir + n) % 4;
                break;
        }
    }

    /**
     * Vrati typ kamene
     *
     * @return typ kamene
     */
    public String getType() {
        return this.type;
    }
    
    /**
     * Vrati otoceni karty
     * @return natoceni karty
     */
    public int getDir() {
        return this.dir;
    }
    
    /**
     * Vraci poklad
     *
     * @return poklad
     */
    public TreasureCard getTreasure() {
        return treasure;
    }

    /**
     * Vlozi poklad
     *
     * @param c poklad
     */
    public void putTreasure(TreasureCard c) {
        this.treasure = c;
    }

}
