/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.project.treasure;

import java.io.Serializable;
import java.util.Random;

/**
 *
 * @author Martin Zemek
 */
public class CardPack implements Serializable {

    /**
     * Aktualni pocet karet v balicku
     */
    private int top;
    private int size;
    private final TreasureCard[] pack;

    /**
     * Konstruktor vytvori balicek o velikosti initSize, alokuje maxSize
     * @param maxSize alokovano
     * @param initSize pouzito
     */
    public CardPack(int maxSize, int initSize) {
        this.pack = new TreasureCard[maxSize];
        if (maxSize >= initSize) {
            for (int i = 0; i < initSize; i++) {
                pack[i] = new TreasureCard(Treasure.getTreasure(i));
            }
            this.top = initSize;
            this.size = this.top;
        }
    }

    /**
     * Vyjme vrchni kartu z balicku.
     *
     * @return vrchni karta
     */
    public TreasureCard popCard() {
        if (this.top > 0) {
            return this.pack[this.size - this.top--];
        } else {
            return null;
        }
    }
    
    /**
     *  Vlozi kartu do balicku
     * @param item karta 
     */
    public void pushCard(TreasureCard item){
        this.top++;
        this.pack[this.top] = item;
    }
    
    /**
     *  
     * @return aktualni pocet v balicku
     */
    public int size() {
        return this.top;
    }

    /**
     * Promicha kartami v balicku
     */
    public void shuffle() {
        Random rnd = new Random();
        for (int i = this.pack.length - 1; i > 0; i--) {
            int index = rnd.nextInt(i + 1);
            TreasureCard tmp = pack[index];
            pack[index] = pack[i];
            pack[i] = tmp;
        }

    }

    /**
     * 
     * @return velikost puvodniho balicku
     */
    public int getSize() {
        return size;
    }

    
}
