/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.project.treasure;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Martin Zemek
 */
public class TreasureCard implements Serializable{

    private final Treasure treasure;

    /**
     * Konstruktor
     * @param tr poklad
     */
    public TreasureCard(Treasure tr) {
        this.treasure = tr;
    }

    /**
     * 
     * @return hash 
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.treasure);
        return hash;
    }

    /**
     * 
     * @param obj objekt
     * @return vysledek
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TreasureCard other = (TreasureCard) obj;
        if (!this.treasure.equals(other.treasure)) {
            return false;
        }
        return true;
    }
    
    /**
     * vrati kod karty
     * @return kod karty
     */
    public int getTCcode(){
        return this.treasure.getCode();
    }

}
