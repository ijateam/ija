/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.project.treasure;

import java.io.Serializable;

/**
 *
 * @author Martin Zemek
 */
public class Treasure implements Serializable {

    private final int code;
    private static final int count = 24;
    private static final Treasure[] treasures = new Treasure[count];

    /**
     * 
     * @param code kod pokladu
     */
    private Treasure(int code) {
        this.code = code;
    }

    /**
     * Vytvori mnozinu pokladu
     */
    public static void createSet() {

        for (int i = 0; i < Treasure.count; i++) {
            treasures[i] = new Treasure(i);
        }
    }

    /**
     * Ziska poklad podle kodu
     * @param code kod pokladu
     * @return poklad 
     */
    public static Treasure getTreasure(int code) {
        for (int i = 0; i < Treasure.count; i++) {
            if (treasures[i].code == code) {
                return treasures[i];
            }
        }
        return null;
    }

    /**
     * 
     * @return kod pokladu
     */
    public int getCode() {
        return this.code;
    }
    
    /**
     * 
     * @return hash 
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + this.code;
        return hash;
    }

    /**
     * 
     * @param obj objekt
     * @return boolean vysledek
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Treasure other = (Treasure) obj;
        if (this.code != other.code) {
            return false;
        }
        return true;
    }

}
