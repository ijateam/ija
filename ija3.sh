#!/bin/sh
javac -d ./build src/ija/homework3/board/*.java src/ija/homework3/game/Main.java
ulimit -v unlimited
jar -cfe dest-client/ija2015-client.jar ija.homework3.game.Main -C build .
java -jar dest-client/ija2015-client.jar
