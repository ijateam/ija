zadání:

https://wis.fit.vutbr.cz/FIT/st/cwk.php?title=IJA:Zad%E1n%ED_projektu&csid=552125&id=9976

Specifikace požadavků
* grafické uživatelské rozhraní (GUI)
* GUI obsahuje zejména menu, hrací desku, prostor pro zobrazení aktuální hrací karty, získaných karet a volného kamene
* graficky musí být jasně odlišitelné kameny, předměty a figurky a také informace, který hráč je právě na tahu

hra
* každá hra má svého lídra, který hru zakládá nebo načítá

* založení nové hry

* lídr zadá velikost hrací desky N, počet úkolových karet K a počet hráčů P (omezení na hodnoty viz výše)

* každému hráči se přidělí unikátní barva figurky (v rámci hry) a umístí se na volné startovní pole

* uložení a načtení hry

* hru je možné kdykoliv uložit do souboru a opět načíst

* po načtení se pokračuje ze stejného místa před uložením

* hráčům se přidělí figurky ve hře náhodně

* po ukončení hry zobrazuje klient stav hry

Varianta A: pouze lokální hra
aplikace umožňuje postupně ovládat všechny hráče ve hře
hry se ukládají lokálně
implementujte operaci undo - možnost vrátit provedené kroky zpět